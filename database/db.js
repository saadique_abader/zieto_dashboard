const Sequelize = require("sequelize");
const db = {};
const sequelize = new Sequelize("Lord_Vader", "root", "mogammad", {
  host: "storm.zieto.co.za",
  dialect: "mysql",
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
