import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Navbar from "./components/Navbar";
import Landing from "./components/Landing";
import Login from "./components/Login";
import Register from "./components/Register";
import Profile from "./components/Profile";
import Dashboard from "./components/Dashboard";
import LandingLogin from "./components/LandingLogin";
import SubmissionLookup from "./tables/SubmissionLookup";
import CorrespondenceLookup from "./tables/CorrespondenceLookup";
import VendorSubmissionCounts from "./tables/VendorSubmissionCounts";
import DataLoadDetail from "./tables/DataLoadDetail";
import DataSyncCheck from "./tables/DataSyncCheck";
import SubmissionDetail from "./tables/SubmissionDetail";
import DataLoadLookup from "./tables/DataLoadLookup";
class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Navbar />
          <Route exact path="/" component={Landing} />
          <div className="container">
            <Route exact path="/home" component={LandingLogin} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/profile" component={Profile} />
            <Route exact path="/dashboard" component={Dashboard} />
            <Route
              exact
              path="/submissionlookup"
              component={SubmissionLookup}
            />
            <Route
              exact
              path="/correspondencelookup"
              component={CorrespondenceLookup}
            />
            <Route
              exact
              path="/vendorsubmissioncounts"
              component={VendorSubmissionCounts}
            />
            <Route exact path="/dataloaddetail" component={DataLoadDetail} />
            <Route exact path="/datasynccheck" component={DataSyncCheck} />
            <Route
              exact
              path="/submissiondetail"
              component={SubmissionDetail}
            />
            <Route exact path="/dataloadlookup" component={DataLoadLookup} />
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
