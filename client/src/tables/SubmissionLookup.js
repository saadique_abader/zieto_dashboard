import React, { Component } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";

class SubmissionLookup extends Component {
  render() {
    return (
      <Form>
        <br />
        <h2>Submission Lookup</h2>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Service Provider ID
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Service Provider ID"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Reference Number
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Reference Number"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Destination ID
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Destination ID"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Vendor ID
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder="Choose Vendor ID">
              <option />
              <option value="ZAFA">Aid for AIDS</option>
              <option value="ZALIEN">Alien Software</option>
              <option value="ZAMS2000">Alliance Management System</option>
              <option value="ZAMPATH">Ampath</option>
              <option value="ZAPACE">Apace</option>
              <option value="ZAPOLLO">Apollo</option>
              <option value="ZBI">BI Solutions</option>
              <option value="BESTMED">Bestmed</option>
              <option value="ZBRITTON">
                Britton Development Services (PTY)LTD
              </option>
              <option value="ZCWP">Cape Western Province</option>
              <option value="ZCARECROSS">Carecross</option>
              <option value="ZCARECROSSME">Carecross MME</option>
              <option value="ZCMD">Chronic Medicine Dispensary</option>
              <option value="ZCLICKS">Clicks</option>
              <option value="ZCOMPH">Compharm</option>
              <option value="ZCOMPUTA">Computassist</option>
              <option value="ZCKS">Computerkit (CKS)</option>
              <option value="ZDATAMAX">Datamax</option>
              <option value="ZDENISI">Denis Information</option>
              <option value="ZDENIS">
                Dental Information Systems (Pty) Ltd
              </option>
              <option value="ZDRC">Dental Risk Company</option>
              <option value="ZDHSWITCH">Digital Healthcare Switch</option>
              <option value="ZDMED">Direct Medicines</option>
              <option value="ZMARCONI">Diverse IT Marconi</option>
              <option value="ZEMD">E-md</option>
              <option value="ZEMC">Emerging Marcet Healthcare</option>
              <option value="ZEUASSIST">Europ Assistance</option>
              <option value="ZFLOWCENTRIC">FlowCentric</option>
              <option value="ZGOODX">Goodx</option>
              <option value="ZHEALTHFOCUS">Health Focus</option>
              <option value="ZPMS263">Health263</option>
              <option value="ZHBRIDGE">Healthbridge</option>
              <option value="ZICW">ICW Consultants CC</option>
              <option value="ZIKAT">IKAT</option>
              <option value="ZISOLESO">Iso Leso Optics Limited</option>
              <option value="KEYHLTH">Keyhealth</option>
              <option value="ZLANCET">Lancet Laboratories</option>
              <option value="ZLENASIA">Lenasia Computer Services</option>
              <option value="ZLIBERATE">Liberate</option>
              <option value="ZLCAPTURE">Liberate Capture</option>
              <option value="ZLHC">Life HealthCare</option>
              <option value="ZMASSMARKET">Mass Market</option>
              <option value="MCC">Medi-Clinic</option>
              <option value="ZMPOST">MediPost</option>
              <option value="ZMEDEDI">Medical EDI Services</option>
              <option value="MSO">Medical Services Organisation</option>
              <option value="ZMEDIKREDIT">Medikredit</option>
              <option value="ZMEDISCOR">Mediscor PBM</option>
              <option value="ZMEDISWITCH">Mediswitch</option>
              <option value="ZMEDWARE">Medware</option>
              <option value="ZMELOMED">Melomed</option>
              <option value="Z5802881">Melomed Practice 5802881</option>
              <option value="Z5807948">Melomed Practice 5807948</option>
              <option value="Z5808103">Melomed Practice 5808103</option>
              <option value="Z5808421">Melomed Practice 5808421</option>
              <option value="Z5808901">Melomed Practice 5808901</option>
              <option value="ZMOTION">Motion Health</option>
              <option value="ZMPHARMA">Mpharma</option>
              <option value="MULT">Multimed</option>
              <option value="NHA">NHA Testing (Saturn)</option>
              <option value="ZRENALCARE">National Renal Care</option>
              <option value="ZNETPH">Netcare Pharmacies</option>
              <option value="ZOCLEAR">Opticlear</option>
              <option value="ZOPHARM">Optipharm (Pty) Ltd</option>
              <option value="ZPULSE">PULSE</option>
              <option value="ZPHARMASYS">PharmaSys</option>
              <option value="ZPSOFT">Pharmasoft</option>
              <option value="ZPRIMECURE">Primecure</option>
              <option value="ZPROPH">Pro-Pharm</option>
              <option value="PMSA">Profmed</option>
              <option value="ZPPN">Provider Private Negotiators</option>
              <option value="ZQUANTSOL">Quant Solutions</option>
              <option value="ZRISKCEDE">Riskcede</option>
              <option value="ZDISPWARE">Robust Systems International</option>
              <option value="ZSMART">SMART</option>
              <option value="ZSMAST">
                Scriptmaster (DJ Lea &amp; Associates)
              </option>
              <option value="ZSPECDENT">Spectra Dental</option>
              <option value="ZEASYRX">Super Scripts (Easy Rx)</option>
              <option value="ZSYNAXON">Synaxon</option>
              <option value="ZCAPS">
                Techknowledge Retail Systems (Comp / Caps)
              </option>
              <option value="ZTFOUR">Trifour Systems</option>
              <option value="VMED">VMed</option>
              <option value="ZVERIPATH">Veripath Health Risk Management</option>
              <option value="ZVERIRAD">Verirad Health Risk Management</option>
              <option value="ZVIRTUAL">Virtual Care</option>
              <option value="ZWEB">Web</option>
              <option value="ZESAY">eSAY Solutions</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Member Number
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Member Number"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            External Number
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter External Number"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Authorisation Number
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Authorisation Number"
            />
          </Col>
        </Form.Group>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Administrator
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option value="1">De Beers Benefit Society</option>
              <option value="11">Momentum Africa</option>
              <option value="14">OHI</option>
              <option value="12">Pro Sano</option>
              <option value="10">Profmed Medical Scheme Administrator</option>
              <option value="6">Sanlam Health</option>
              <option value="3">
                Spes Bona Financial Administrators (Pty) Ltd.
              </option>
              <option value="5">VMED</option>
              <option value="9">VMED Africa</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Scheme
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option value="17">Alexander Forbes Kenya</option>
              <option value="7">Bestmed</option>
              <option value="24">Blue Namibia</option>
              <option value="35">Blue Uganda</option>
              <option value="34">Blue Zambia</option>
              <option value="33">Blue Zimbabwe</option>
              <option value="18">CFCLife Health</option>
              <option value="28">Corporate Blue Mozambique</option>
              <option value="30">Emose Health Plan Mozambique</option>
              <option value="31">Essential Health Malawi</option>
              <option value="16">Heritage Insurance Company</option>
              <option value="20">LHH Malawi</option>
              <option value="10">Libcare</option>
              <option value="21">Liberty Blue</option>
              <option value="29">Liberty Health Mozambique</option>
              <option value="39">OHI</option>
              <option value="22">Optimum Global Africa Plan</option>
              <option value="25">Pro Sano Medical Scheme</option>
              <option value="12">Profmed</option>
              <option value="15">SICOM Health Plan - Mautitius </option>
              <option value="3">Spes Bona</option>
              <option value="26">Total Health Trust</option>
              <option value="13">Ugamed</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Scheme Plan
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option value="1">Libcare</option>
              <option value="2">Liberty</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Scheme Plan Option
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option value="8">Gold Complete</option>
              <option value="5">Gold Focus</option>
              <option value="9">Gold Plus</option>
              <option value="1">Libcare Agents</option>
              <option value="2">Libcare Staff</option>
              <option value="4">Platinum Complete</option>
              <option value="3">Platinum Focus</option>
              <option value="10">Platinum Plus</option>
              <option value="7">Silver Corporate Network</option>
              <option value="6">Silver Focus</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Client
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option value="8">Liberty Retail</option>
              <option value="2">Medi-Clinic</option>
              <option value="3">PPN</option>
              <option value="6">Prime Cure Lonmin</option>
              <option value="10">Smart</option>
              <option value="7">Zieto</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Status
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option value="E">E-Error</option>
              <option value="F">F-Failure</option>
              <option value="P">P-Pending</option>
              <option value="S">S-Success</option>
              <option value="E">E-Error</option>
              <option value="P">P-Pending</option>
              <option value="S">S-Success</option>
              <option value="F">F-Failure</option>
              <option value="S">S-Success</option>
              <option value="A">A-Approved</option>
              <option value="D">D-Pended</option>
              <option value="D">D-Pended</option>
              <option value="O">O-Approved with 0 response values</option>
              <option value="P">P-Partially Approved</option>
              <option value="R">R-Rejected</option>
              <option value="X">X-Reversed</option>
              <option value="Y">Y-Unsuccessful Reversal</option>
              <option value="Z">Z-Successful Reversal</option>
              <option value="Unknown">Unknown Status</option>
              <option value="NULL">No Status</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Site
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option value="CTN">Cape Town</option>
              <option value="JHB">Johannesburg</option>
              <option value="PTA">Pretoria</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Country Code
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option value="AF">Afghanistan</option>
              <option value="AL">Albania</option>
              <option value="DZ">Algeria</option>
              <option value="AS">American Samoa</option>
              <option value="AD">Andorra</option>
              <option value="AO">Angola</option>
              <option value="AI">Anguilla</option>
              <option value="AQ">Antarctica</option>
              <option value="AG">Antigua and Barbuda</option>
              <option value="AR">Argentinia</option>
              <option value="AM">Armenia</option>
              <option value="AW">Aruba</option>
              <option value="AU">Australia</option>
              <option value="AT">Austria</option>
              <option value="AZ">Azerbaijan</option>
              <option value="BS">Bahamas</option>
              <option value="BH">Bahrain</option>
              <option value="BD">Bangladesh</option>
              <option value="BB">Barbados</option>
              <option value="BY">Belarus</option>
              <option value="BE">Belgium</option>
              <option value="BZ">Belize</option>
              <option value="BJ">Benin</option>
              <option value="BM">Bermuda</option>
              <option value="BT">Bhutan</option>
              <option value="BO">Bolivia</option>
              <option value="BA">Bosnia and Herzegowina</option>
              <option value="BW">Botswana</option>
              <option value="BR">Brazil</option>
              <option value="BN">Brunei Darussalam</option>
              <option value="BG">Bulgaria</option>
              <option value="BF">Burkina Faso</option>
              <option value="BI">Burundi</option>
              <option value="KH">Cambodia</option>
              <option value="CM">Cameroon</option>
              <option value="CA">Canada</option>
              <option value="CV">Cape Verde</option>
              <option value="KY">Cayman Islands</option>
              <option value="CF">Central African Republic</option>
              <option value="TD">Chad</option>
              <option value="CL">Chile</option>
              <option value="CN">China</option>
              <option value="CX">Christmas Island</option>
              <option value="CC">Cocos (Keeling) Islands</option>
              <option value="CO">Colombia</option>
              <option value="KM">Comoros</option>
              <option value="CG">Congo</option>
              <option value="CK">Cook Islands</option>
              <option value="CR">Costa Rica</option>
              <option value="CI">Cote D'IVoire</option>
              <option value="HR">Croatia</option>
              <option value="CU">Cuba</option>
              <option value="CY">Cyprus</option>
              <option value="CZ">Czech Republic</option>
              <option value="DK">Denmark</option>
              <option value="DJ">Djibouti</option>
              <option value="DM">Dominica</option>
              <option value="DO">Dominican Republic</option>
              <option value="TP">East Timor</option>
              <option value="EC">Ecuador</option>
              <option value="EG">Egypt</option>
              <option value="SV">El Salvador</option>
              <option value="GQ">Equatorial Guinea</option>
              <option value="ER">Eritrea</option>
              <option value="EE">Estonia</option>
              <option value="ET">Ethiopia</option>
              <option value="FK">Falkland Islands (Malvinas)</option>
              <option value="FO">Faroe Islands</option>
              <option value="FJ">Fiji</option>
              <option value="FI">Finland</option>
              <option value="FR">France</option>
              <option value="GF">French Guiana</option>
              <option value="PF">French Polynesia</option>
              <option value="GA">Gabon</option>
              <option value="GM">Gambia</option>
              <option value="GE">Georgia</option>
              <option value="DE">Germany</option>
              <option value="GH">Ghana</option>
              <option value="GI">Gibraltar</option>
              <option value="GR">Greece</option>
              <option value="GL">Greenland</option>
              <option value="GD">Grenada</option>
              <option value="GP">Guadeloupe</option>
              <option value="GU">Guam</option>
              <option value="GT">Guatamala</option>
              <option value="GN">Guinea</option>
              <option value="GW">Guinea-Bissau</option>
              <option value="GY">Guyana</option>
              <option value="HT">Haiti</option>
              <option value="HN">Honduras</option>
              <option value="HK">Hong Kong</option>
              <option value="HU">Hungary</option>
              <option value="IS">Iceland</option>
              <option value="IN">India</option>
              <option value="ID">Indonesia</option>
              <option value="IR">Iran</option>
              <option value="IQ">Iraq</option>
              <option value="IE">Ireland</option>
              <option value="IL">Israel</option>
              <option value="IT">Italy</option>
              <option value="JM">Jamaica</option>
              <option value="JP">Japan</option>
              <option value="JO">Jordan</option>
              <option value="KZ">Kazakhstan</option>
              <option value="KE">Kenya</option>
              <option value="KI">Kiribati</option>
              <option value="KP">Korea, Democratic People's Republic of</option>
              <option value="KR">Korea, Republic of</option>
              <option value="KW">Kuwait</option>
              <option value="KG">Kyrgyzstan</option>
              <option value="LA">Lao People's Democratic Republic</option>
              <option value="LV">Latvia</option>
              <option value="LB">Lebanon</option>
              <option value="LS">Lesotho</option>
              <option value="LR">Liberia</option>
              <option value="LY">Libyan Arab Jamahiriya</option>
              <option value="LI">Liechtenstein</option>
              <option value="LT">Lithuania</option>
              <option value="LU">Luxembourg</option>
              <option value="MO">Macau</option>
              <option value="MK">Macedonia</option>
              <option value="MG">Madagascar</option>
              <option value="MW">Malawi</option>
              <option value="MY">Malaysia</option>
              <option value="MV">Maldives</option>
              <option value="ML">Mali</option>
              <option value="MT">Malta</option>
              <option value="MH">Marshall Islands</option>
              <option value="MQ">Martinique</option>
              <option value="MR">Mauritiana</option>
              <option value="MU">Mauritius</option>
              <option value="YT">Mayotte</option>
              <option value="MX">Mexico</option>
              <option value="FM">Micronesia</option>
              <option value="MD">Moldova</option>
              <option value="MC">Monaco</option>
              <option value="MN">Mongolia</option>
              <option value="MS">Montserrat</option>
              <option value="MA">Morocco</option>
              <option value="MZ">Mozambique</option>
              <option value="MM">Myanmar</option>
              <option value="NA">Namibia</option>
              <option value="NR">Nauro</option>
              <option value="NP">Nepal</option>
              <option value="NL">Netherlands</option>
              <option value="AN">Netherlands Antilles</option>
              <option value="NC">New Caledonia</option>
              <option value="NZ">New Zealand</option>
              <option value="NI">Nicaragua</option>
              <option value="NE">Niger</option>
              <option value="NG">Nigeria</option>
              <option value="NU">Niue</option>
              <option value="NF">Norfolk Island</option>
              <option value="MP">Northern Mariana Islands</option>
              <option value="NO">Norway</option>
              <option value="OM">Oman</option>
              <option value="PK">Pakistan</option>
              <option value="PW">Palau</option>
              <option value="PA">Panama</option>
              <option value="PG">Papau New Guinea</option>
              <option value="PY">Paraguay</option>
              <option value="PE">Peru</option>
              <option value="PR">Peurto Rico</option>
              <option value="PH">Philippines</option>
              <option value="PL">Poland</option>
              <option value="PT">Portugal</option>
              <option value="QA">Qatar</option>
              <option value="RE">Reunion</option>
              <option value="RO">Romania</option>
              <option value="RU">Russian Federation</option>
              <option value="RW">Rwanda</option>
              <option value="KN">Saint Kitts and Nevis</option>
              <option value="LC">Saint Lucia</option>
              <option value="VC">Saint Vincent and the Grenadines</option>
              <option value="WS">Samoa</option>
              <option value="SM">San Marino</option>
              <option value="ST">Sao Tome and Principe</option>
              <option value="SA">Saudi Arabia</option>
              <option value="SN">Senegal</option>
              <option value="SC">Seychelles</option>
              <option value="SL">Sierra Leone</option>
              <option value="SG">Singapore</option>
              <option value="SK">Slovakia</option>
              <option value="SI">Slovenia</option>
              <option value="SB">Solomon Islands</option>
              <option value="SO">Somalia</option>
              <option value="ZA">South Africa</option>
              <option value="SS">South Sudan</option>
              <option value="ES">Spain</option>
              <option value="LK">Sri Lanka</option>
              <option value="PM">St Pierre and Miquelon</option>
              <option value="SH">St. Helena</option>
              <option value="SD">Sudan</option>
              <option value="SR">Suriname</option>
              <option value="SZ">Swaziland</option>
              <option value="SE">Sweden</option>
              <option value="CH">Switzerland</option>
              <option value="SY">Syrian Arab Republic</option>
              <option value="TW">Taiwan</option>
              <option value="TJ">Tajikistan</option>
              <option value="TZ">Tanzania</option>
              <option value="TH">Thailand</option>
              <option value="CD">The Democratic Republic of the Congo</option>
              <option value="TG">Togo</option>
              <option value="TK">Tokelau</option>
              <option value="TO">Tonga</option>
              <option value="TT">Trinidad and Tobago</option>
              <option value="TN">Tunisia</option>
              <option value="TR">Turkey</option>
              <option value="TM">Turkmenistan</option>
              <option value="TC">Turks and Caicos Islands</option>
              <option value="TV">Tuvalu</option>
              <option value="UG">Uganda</option>
              <option value="UA">Ukraine</option>
              <option value="AE">United Arab Emirates</option>
              <option value="GB">United Kingdom</option>
              <option value="US">United States</option>
              <option value="UN">Unknown</option>
              <option value="UY">Uruguay</option>
              <option value="UZ">Uzbekistan</option>
              <option value="VU">Vanautu</option>
              <option value="VA">Vatican City State</option>
              <option value="VE">Venezuela</option>
              <option value="VN">Vietnam</option>
              <option value="VG">Virgin Islands (British)</option>
              <option value="VI">Virgin Islands (U.S.)</option>
              <option value="WF">Wallis and Futuna Islands</option>
              <option value="YE">Yemen</option>
              <option value="YU">Yugoslavia</option>
              <option value="ZR">Zaire</option>
              <option value="ZM">Zambia</option>
              <option value="ZW">Zimbabwe</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Batch Number
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Batch Number"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Batch Date
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Batch Date"
            />
          </Col>
          Format: YYYY-MM-DD OR YYYY-MM-DD hh:mm:ss
        </Form.Group>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            From Date
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Batch Date"
            />
          </Col>
          * Format: YYYY-MM-DD OR YYYY-MM-DD hh:mm:ss
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            To Date
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Batch Date"
            />
          </Col>
          * Format: YYYY-MM-DD OR YYYY-MM-DD hh:mm:ss
        </Form.Group>

        <Button variant="info" type="submit">
          Lookup
        </Button>
        <hr />
      </Form>
    );
  }
}

export default SubmissionLookup;
