import React, { Component } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";

class DataLoadLookup extends Component {
  render() {
    return (
      <Form>
        <br />
        <h2>Data Load Lookup</h2>
        <br />
        <div>
          Please enter the details of the data load you wish to look up:
        </div>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Category
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option value="AUTH">AUTH</option>
              <option value="BHF">BHF</option>
              <option value="BHFDISP">BHFDISP</option>
              <option value="CLAIM">CLAIM</option>
              <option value="CORRESPONDENCE">CORRESPONDENCE</option>
              <option value="ENQUIRIES">ENQUIRIES</option>
              <option value="ERA">ERA</option>
              <option value="FINANCE">FINANCE</option>
              <option value="GEO">GEO</option>
              <option value="MEMBER">MEMBER</option>
              <option value="NAP">NAP</option>
              <option value="REPORT">REPORT</option>
              <option value="SCHEME">SCHEME</option>
              <option value="STATEMENT">STATEMENT</option>
            </Form.Control>
          </Col>
          *
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Originator
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option value="AFRIGIS">AFRIGIS</option>
              <option value="ALIVE">ALIVE</option>
              <option value="AMPATH">AMPATH</option>
              <option value="APACE">APACE</option>
              <option value="BHF">BHF</option>
              <option value="BI">BI</option>
              <option value="BESTMED">BESTMED</option>
              <option value="BRITTON">BRITTON</option>
              <option value="CDE">CDE</option>
              <option value="CKS">CKS</option>
              <option value="CARECROSS">CARECROSS</option>
              <option value="CAREWARE">CAREWARE</option>
              <option value="CLICKS">CLICKS</option>
              <option value="CLINIX">CLINIX</option>
              <option value="COMPUTASSIST">COMPUTASSIST</option>
              <option value="DHS">DHS</option>
              <option value="DRC">DRC</option>
              <option value="DATAMAX">DATAMAX</option>
              <option value="DBBS">DBBS</option>
              <option value="DART">DART</option>
              <option value="DENIS">DENIS</option>
              <option value="DIGIPATH">DIGIPATH</option>
              <option value="DIRECTAXIS">DIRECTAXIS</option>
              <option value="EMC">EMC</option>
              <option value="EUROPASSIST">EUROPASSIST</option>
              <option value="HEALTHCYBRARIAN">HEALTHCYBRARIAN</option>
              <option value="HEALTHBRIDGE">HEALTHBRIDGE</option>
              <option value="ICW">ICW</option>
              <option value="IKAT">IKAT</option>
              <option value="IPAF">IPAF</option>
              <option value="INCE">INCE</option>
              <option value="INVESTMED">INVESTMED</option>
              <option value="ISOLESO">ISOLESO</option>
              <option value="KEYHEALTH">KEYHEALTH</option>
              <option value="LMS">LMS</option>
              <option value="LANCET">LANCET</option>
              <option value="LENASIA">LENASIA</option>
              <option value="LIBCARE">LIBCARE</option>
              <option value="LIBERTY">LIBERTY</option>
              <option value="LHAFRICA">LHAFRICA</option>
              <option value="BLUEAO">BLUEAO</option>
              <option value="BLUEGH">BLUEGH</option>
              <option value="BLUEKE">BLUEKE</option>
              <option value="BLUEMW">BLUEMW</option>
              <option value="BLUEMU">BLUEMU</option>
              <option value="BLUEMZ">BLUEMZ</option>
              <option value="BLUENA">BLUENA</option>
              <option value="BLUENG">BLUENG</option>
              <option value="BLUEZA">BLUEZA</option>
              <option value="BLUEUG">BLUEUG</option>
              <option value="BLUEZM">BLUEZM</option>
              <option value="BLUEZW">BLUEZW</option>
              <option value="LH">LH</option>
              <option value="LIBERTYINS">LIBERTYINS</option>
              <option value="LIBRETAIL">LIBRETAIL</option>
              <option value="LIFEHC">LIFEHC</option>
              <option value="LONMIN">LONMIN</option>
              <option value="MEDPAGES">MEDPAGES</option>
              <option value="MASSMARKET">MASSMARKET</option>
              <option value="MEDEDI">MEDEDI</option>
              <option value="MEDICLINIC">MEDICLINIC</option>
              <option value="MEDICOVER">MEDICOVER</option>
              <option value="MEDIKREDITP">MEDIKREDITP</option>
              <option value="MEDIKREDITX">MEDIKREDITX</option>
              <option value="MEDIKREDITH">MEDIKREDITH</option>
              <option value="MEDIKREDITD">MEDIKREDITD</option>
              <option value="MEDIKREDIT">MEDIKREDIT</option>
              <option value="MEDILINK">MEDILINK</option>
              <option value="MEDISCOR">MEDISCOR</option>
              <option value="MEDISWITCH">MEDISWITCH</option>
              <option value="MDS">MDS</option>
              <option value="MELOMED">MELOMED</option>
              <option value="MPHARMA">MPHARMA</option>
              <option value="NHA">NHA</option>
              <option value="NHN">NHN</option>
              <option value="NETCARE">NETCARE</option>
              <option value="OMHC">OMHC</option>
              <option value="OPTICLEAR">OPTICLEAR</option>
              <option value="OPTIMUMGLOBAL">OPTIMUMGLOBAL</option>
              <option value="OYLREWARDS">OYLREWARDS</option>
              <option value="PPN">PPN</option>
              <option value="PATHCARE">PATHCARE</option>
              <option value="PRIMECURE">PRIMECURE</option>
              <option value="PROSANO">PROSANO</option>
              <option value="PMSA">PMSA</option>
              <option value="PROFMED">PROFMED</option>
              <option value="PULSE">PULSE</option>
              <option value="RFMCF">RFMCF</option>
              <option value="RENALCARE">RENALCARE</option>
              <option value="RISKCEDE">RISKCEDE</option>
              <option value="SED">SED</option>
              <option value="SELFMED">SELFMED</option>
              <option value="SMART">SMART</option>
              <option value="SARS">SARS</option>
              <option value="SPECTRADENTAL">SPECTRADENTAL</option>
              <option value="SPECTRAMED">SPECTRAMED</option>
              <option value="SPESBONA">SPESBONA</option>
              <option value="SYNAXON">SYNAXON</option>
              <option value="TELEMED">TELEMED</option>
              <option value="TELEMEDB">TELEMEDB</option>
              <option value="TELEMEDG">TELEMEDG</option>
              <option value="TOYOTAZL">TOYOTAZL</option>
              <option value="TRACKER">TRACKER</option>
              <option value="TRIFOUR">TRIFOUR</option>
              <option value="UAP">UAP</option>
              <option value="VWZL">VWZL</option>
              <option value="VERIPATH">VERIPATH</option>
              <option value="VERIRAD">VERIRAD</option>
              <option value="VINNWARE">VINNWARE</option>
              <option value="VMED">VMED</option>
              <option value="WESBANK">WESBANK</option>
              <option value="WESBANKZL">WESBANKZL</option>
              <option value="WESTRACK">WESTRACK</option>
              <option value="ZESTLIFE">ZESTLIFE</option>
              <option value="ZIETO">ZIETO</option>
              <option value="EMD">EMD</option>
              <option value="JOULES">JOULES</option>
            </Form.Control>
          </Col>
          *
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Destination
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option value="AFA">AFA</option>
              <option value="AFRIGIS">AFRIGIS</option>
              <option value="AMPATH">AMPATH</option>
              <option value="APACE">APACE</option>
              <option value="BI">BI</option>
              <option value="BESTMED">BESTMED</option>
              <option value="JUSTREWARDS">JUSTREWARDS</option>
              <option value="WELLNESS">WELLNESS</option>
              <option value="BIDVEST">BIDVEST</option>
              <option value="BRITTON">BRITTON</option>
              <option value="CKS">CKS</option>
              <option value="CARECROSS">CARECROSS</option>
              <option value="CIMOS">CIMOS</option>
              <option value="CLICKS">CLICKS</option>
              <option value="COMPHARM">COMPHARM</option>
              <option value="COMPUTASSIST">COMPUTASSIST</option>
              <option value="CONNECTDIRECT">CONNECTDIRECT</option>
              <option value="CMS">CMS</option>
              <option value="DHS">DHS</option>
              <option value="DRC">DRC</option>
              <option value="DATAMAX">DATAMAX</option>
              <option value="DBBS">DBBS</option>
              <option value="DENIS">DENIS</option>
              <option value="DIGIPATH">DIGIPATH</option>
              <option value="DIRECTMEDICINES">DIRECTMEDICINES</option>
              <option value="EMC">EMC</option>
              <option value="ER24">ER24</option>
              <option value="EBONYIVORY">EBONYIVORY</option>
              <option value="EUROPASSIST">EUROPASSIST</option>
              <option value="FIFTHQUADRANT">FIFTHQUADRANT</option>
              <option value="HARDRAIN">HARDRAIN</option>
              <option value="HEALTHMONITOR">HEALTHMONITOR</option>
              <option value="HEALTHBRIDGE">HEALTHBRIDGE</option>
              <option value="HEALTHICHOICES">HEALTHICHOICES</option>
              <option value="ICW">ICW</option>
              <option value="INQUBA">INQUBA</option>
              <option value="IKAT">IKAT</option>
              <option value="INCE">INCE</option>
              <option value="INVESTMED">INVESTMED</option>
              <option value="ISOLESO">ISOLESO</option>
              <option value="KEYHEALTH0">KEYHEALTH0</option>
              <option value="KEYHEALTH">KEYHEALTH</option>
              <option value="KEYHEALTH19">KEYHEALTH19</option>
              <option value="KEYHEALTH14">KEYHEALTH14</option>
              <option value="KEYHEALTH7">KEYHEALTH7</option>
              <option value="KOH">KOH</option>
              <option value="LMS">LMS</option>
              <option value="LANCET">LANCET</option>
              <option value="LASERCOM">LASERCOM</option>
              <option value="LENASIA">LENASIA</option>
              <option value="LESOTHO">LESOTHO</option>
              <option value="LIBCARE">LIBCARE</option>
              <option value="LIBERATE">LIBERATE</option>
              <option value="LIBERTY">LIBERTY</option>
              <option value="LHAFRICA">LHAFRICA</option>
              <option value="BLUE">BLUE</option>
              <option value="BLUEAO">BLUEAO</option>
              <option value="BLUEGH">BLUEGH</option>
              <option value="BLUEMW">BLUEMW</option>
              <option value="BLUEMU">BLUEMU</option>
              <option value="BLUEMZ">BLUEMZ</option>
              <option value="BLUENA">BLUENA</option>
              <option value="BLUENG">BLUENG</option>
              <option value="BLUEZA">BLUEZA</option>
              <option value="BLUEUG">BLUEUG</option>
              <option value="BLUEZM">BLUEZM</option>
              <option value="BLUEZW">BLUEZW</option>
              <option value="LIBRETAIL">LIBRETAIL</option>
              <option value="LIFEHC">LIFEHC</option>
              <option value="LIFESENSE">LIFESENSE</option>
              <option value="MEDSCHEME">MEDSCHEME</option>
              <option value="MSO">MSO</option>
              <option value="MARCONI">MARCONI</option>
              <option value="MASSMARKET">MASSMARKET</option>
              <option value="MEDEDI">MEDEDI</option>
              <option value="MEDICLINIC">MEDICLINIC</option>
              <option value="MEDICOVER">MEDICOVER</option>
              <option value="MEDIKREDIT">MEDIKREDIT</option>
              <option value="MEDILINK">MEDILINK</option>
              <option value="MEDISCOR">MEDISCOR</option>
              <option value="MEDISWITCH">MEDISWITCH</option>
              <option value="MELOMED">MELOMED</option>
              <option value="5802881">5802881</option>
              <option value="5807948">5807948</option>
              <option value="5808103">5808103</option>
              <option value="5808421">5808421</option>
              <option value="5808901">5808901</option>
              <option value="MOBI">MOBI</option>
              <option value="MONTAGE">MONTAGE</option>
              <option value="MPHARMA">MPHARMA</option>
              <option value="OMG">OMG</option>
              <option value="OMT">OMT</option>
              <option value="OMHC">OMHC</option>
              <option value="OPTICLEAR">OPTICLEAR</option>
              <option value="OPTIPHARM">OPTIPHARM</option>
              <option value="OPTIMUMGLOBAL">OPTIMUMGLOBAL</option>
              <option value="OYLREWARDS">OYLREWARDS</option>
              <option value="PPN">PPN</option>
              <option value="PROSORT">PROSORT</option>
              <option value="PATHCARE">PATHCARE</option>
              <option value="PAHCARE">PAHCARE</option>
              <option value="PHARMASYS">PHARMASYS</option>
              <option value="PRIMECURE">PRIMECURE</option>
              <option value="PROSANO">PROSANO</option>
              <option value="PROFMED">PROFMED</option>
              <option value="PMSA">PMSA</option>
              <option value="RFMCF">RFMCF</option>
              <option value="SED">SED</option>
              <option value="SELFMED">SELFMED</option>
              <option value="SARS">SARS</option>
              <option value="SPECTRADENTAL">SPECTRADENTAL</option>
              <option value="SPECTRAMED">SPECTRAMED</option>
              <option value="SUPERSCRIPTS">SUPERSCRIPTS</option>
              <option value="SYNAXON">SYNAXON</option>
              <option value="TECHKNOWLEDGE">TECHKNOWLEDGE</option>
              <option value="TELEMED">TELEMED</option>
              <option value="TOTALMED">TOTALMED</option>
              <option value="UAP">UAP</option>
              <option value="UAPKE">UAPKE</option>
              <option value="UAPRW">UAPRW</option>
              <option value="UAPSS">UAPSS</option>
              <option value="UAPUG">UAPUG</option>
              <option value="UGANDA">UGANDA</option>
              <option value="VERIPATH">VERIPATH</option>
              <option value="VERIRAD">VERIRAD</option>
              <option value="VMED">VMED</option>
              <option value="VMEDAFRICA">VMEDAFRICA</option>
              <option value="WELLNICITY">WELLNICITY</option>
              <option value="ZIETO">ZIETO</option>
              <option value="REPO">REPO</option>
            </Form.Control>
          </Col>
          *
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Sequence Number
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" type="text" placeholder="" />
          </Col>
          *
        </Form.Group>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            From Date
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" type="text" placeholder="" />
          </Col>
          * Format: YYYY-MM-DD OR YYYY-MM-DD hh:mm:ss
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            To Date
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" type="text" placeholder="" />
          </Col>
          * Format: YYYY-MM-DD OR YYYY-MM-DD hh:mm:ss
        </Form.Group>
        <Button variant="info" type="submit">
          Lookup
        </Button>
        <b>
          <hr color="black" />
        </b>
        <div>Please enter the name of the file you wish to find:</div>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Filename
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" type="text" placeholder="" />
          </Col>
          *
        </Form.Group>
        <Button variant="info" type="submit">
          Find in Database
        </Button>
        &nbsp;&nbsp;&nbsp;
        <Button variant="info" type="submit">
          Find on Filesystem
        </Button>
        &nbsp;&nbsp;&nbsp;
        <Button variant="info" type="submit">
          Find in Archive Filesystem
        </Button>
        <hr color="black" />
        <div>Please enter the basic details of what you are searching for:</div>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            FTP User
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" type="text" placeholder="" />
            <option />
          </Col>
          *
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Category
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder="">
              <option />
              <option value="authfile">AUTH</option>
              <option value="claimsfile">CLAIM</option>
              <option value="correspondencefile">CORRESPONDENCE</option>
              <option value="erafile">ERA</option>
              <option value="memfile">MEMBER</option>
              <option value="napfile">NAP</option>
              <option value="schemefile">SCHEME</option>
              <option value="statementfile">STATEMENT</option>
            </Form.Control>
          </Col>
          *
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Text to search
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" type="text" placeholder="" />
            <option />
          </Col>
          *
        </Form.Group>
        <Button variant="info" type="submit">
          Search
        </Button>
        <hr color="black" />
        <div>
          Please enter the details of the data load you wish to find related
          files for:
        </div>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Category
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder="">
              <option />
              <option value="CORRESPONDENCE">CORRESPONDENCE</option>
              <option value="STATEMENT">STATEMENT</option>
            </Form.Control>
          </Col>
          *
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Originator
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder="">
              <option />
              <option value="AFRIGIS">AFRIGIS</option>
              <option value="ALIVE">ALIVE</option>
              <option value="AMPATH">AMPATH</option>
              <option value="APACE">APACE</option>
              <option value="BHF">BHF</option>
              <option value="BI">BI</option>
              <option value="BESTMED">BESTMED</option>
              <option value="BRITTON">BRITTON</option>
              <option value="CDE">CDE</option>
              <option value="CKS">CKS</option>
              <option value="CARECROSS">CARECROSS</option>
              <option value="CAREWARE">CAREWARE</option>
              <option value="CLICKS">CLICKS</option>
              <option value="CLINIX">CLINIX</option>
              <option value="COMPUTASSIST">COMPUTASSIST</option>
              <option value="DHS">DHS</option>
              <option value="DRC">DRC</option>
              <option value="DATAMAX">DATAMAX</option>
              <option value="DBBS">DBBS</option>
              <option value="DART">DART</option>
              <option value="DENIS">DENIS</option>
              <option value="DIGIPATH">DIGIPATH</option>
              <option value="DIRECTAXIS">DIRECTAXIS</option>
              <option value="EMC">EMC</option>
              <option value="EUROPASSIST">EUROPASSIST</option>
              <option value="HEALTHCYBRARIAN">HEALTHCYBRARIAN</option>
              <option value="HEALTHBRIDGE">HEALTHBRIDGE</option>
              <option value="ICW">ICW</option>
              <option value="IKAT">IKAT</option>
              <option value="IPAF">IPAF</option>
              <option value="INCE">INCE</option>
              <option value="INVESTMED">INVESTMED</option>
              <option value="ISOLESO">ISOLESO</option>
              <option value="KEYHEALTH">KEYHEALTH</option>
              <option value="LMS">LMS</option>
              <option value="LANCET">LANCET</option>
              <option value="LENASIA">LENASIA</option>
              <option value="LIBCARE">LIBCARE</option>
              <option value="LIBERTY">LIBERTY</option>
              <option value="LHAFRICA">LHAFRICA</option>
              <option value="BLUEAO">BLUEAO</option>
              <option value="BLUEGH">BLUEGH</option>
              <option value="BLUEKE">BLUEKE</option>
              <option value="BLUEMW">BLUEMW</option>
              <option value="BLUEMU">BLUEMU</option>
              <option value="BLUEMZ">BLUEMZ</option>
              <option value="BLUENA">BLUENA</option>
              <option value="BLUENG">BLUENG</option>
              <option value="BLUEZA">BLUEZA</option>
              <option value="BLUEUG">BLUEUG</option>
              <option value="BLUEZM">BLUEZM</option>
              <option value="BLUEZW">BLUEZW</option>
              <option value="LH">LH</option>
              <option value="LIBERTYINS">LIBERTYINS</option>
              <option value="LIBRETAIL">LIBRETAIL</option>
              <option value="LIFEHC">LIFEHC</option>
              <option value="LONMIN">LONMIN</option>
              <option value="MEDPAGES">MEDPAGES</option>
              <option value="MASSMARKET">MASSMARKET</option>
              <option value="MEDEDI">MEDEDI</option>
              <option value="MEDICLINIC">MEDICLINIC</option>
              <option value="MEDICOVER">MEDICOVER</option>
              <option value="MEDIKREDITP">MEDIKREDITP</option>
              <option value="MEDIKREDITX">MEDIKREDITX</option>
              <option value="MEDIKREDITH">MEDIKREDITH</option>
              <option value="MEDIKREDITD">MEDIKREDITD</option>
              <option value="MEDIKREDIT">MEDIKREDIT</option>
              <option value="MEDILINK">MEDILINK</option>
              <option value="MEDISCOR">MEDISCOR</option>
              <option value="MEDISWITCH">MEDISWITCH</option>
              <option value="MDS">MDS</option>
              <option value="MELOMED">MELOMED</option>
              <option value="MPHARMA">MPHARMA</option>
              <option value="NHA">NHA</option>
              <option value="NHN">NHN</option>
              <option value="NETCARE">NETCARE</option>
              <option value="OMHC">OMHC</option>
              <option value="OPTICLEAR">OPTICLEAR</option>
              <option value="OPTIMUMGLOBAL">OPTIMUMGLOBAL</option>
              <option value="OYLREWARDS">OYLREWARDS</option>
              <option value="PPN">PPN</option>
              <option value="PATHCARE">PATHCARE</option>
              <option value="PRIMECURE">PRIMECURE</option>
              <option value="PROSANO">PROSANO</option>
              <option value="PMSA">PMSA</option>
              <option value="PROFMED">PROFMED</option>
              <option value="PULSE">PULSE</option>
              <option value="RFMCF">RFMCF</option>
              <option value="RENALCARE">RENALCARE</option>
              <option value="RISKCEDE">RISKCEDE</option>
              <option value="SED">SED</option>
              <option value="SELFMED">SELFMED</option>
              <option value="SMART">SMART</option>
              <option value="SARS">SARS</option>
              <option value="SPECTRADENTAL">SPECTRADENTAL</option>
              <option value="SPECTRAMED">SPECTRAMED</option>
              <option value="SPESBONA">SPESBONA</option>
              <option value="SYNAXON">SYNAXON</option>
              <option value="TELEMED">TELEMED</option>
              <option value="TELEMEDB">TELEMEDB</option>
              <option value="TELEMEDG">TELEMEDG</option>
              <option value="TOYOTAZL">TOYOTAZL</option>
              <option value="TRACKER">TRACKER</option>
              <option value="TRIFOUR">TRIFOUR</option>
              <option value="UAP">UAP</option>
              <option value="VWZL">VWZL</option>
              <option value="VERIPATH">VERIPATH</option>
              <option value="VERIRAD">VERIRAD</option>
              <option value="VINNWARE">VINNWARE</option>
              <option value="VMED">VMED</option>
              <option value="WESBANK">WESBANK</option>
              <option value="WESBANKZL">WESBANKZL</option>
              <option value="WESTRACK">WESTRACK</option>
              <option value="ZESTLIFE">ZESTLIFE</option>
              <option value="ZIETO">ZIETO</option>
              <option value="EMD">EMD</option>
              <option value="JOULES">JOULES</option>
            </Form.Control>
          </Col>
          *
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Sequence Number
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" type="text" placeholder="" />
            <option />
          </Col>
          *
        </Form.Group>
        <Button variant="info" type="submit">
          Find
        </Button>
        <hr color="black" />
      </Form>
    );
  }
}

export default DataLoadLookup;
