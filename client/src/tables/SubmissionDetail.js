import React, { Component } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";

class SubmissionDetail extends Component {
  render() {
    return (
      <Form>
        <br />
        <h2>Submission Detail</h2>
        <br />
        <div>Please enter the Zieto ID of the submission you wish to view:</div>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Client ID
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" type="text" placeholder="Enter Client ID" />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Zieto ID
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" type="text" placeholder="Enter Zieto ID" />
          </Col>
        </Form.Group>
        <Button variant="primary" type="submit">
          View
        </Button>
      </Form>
    );
  }
}

export default SubmissionDetail;
