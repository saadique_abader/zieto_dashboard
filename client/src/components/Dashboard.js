import React from "react";

const API = "http://localhost:4000/users";

class Dashboard extends React.Component {
  constructor() {
    super();
    this.state = {
      data: []
    };
  }

  componentDidMount() {
    fetch(API)
      .then(response => response.json())
      .then(response => this.setState({ data: response }))
      .catch(err => console.error(err));
  }

  render() {
    const display = this.state.data.map(obj => {
      return (
        <div>
          <table className="table col-md-3 ">
            <tbody>
              <tr key={obj.id}>
                <td>{obj.con_serial} </td>
                <td>{obj.con_name}</td>
                <td>{obj.con_surname}</td>
                <td>{obj.con_title}</td>
              </tr>
            </tbody>
          </table>
        </div>
      );
    });
    return (
      <div>
        <table className="table col-md-4">
          <thead>
            <tr>
              <th>Serial</th>
              <th>Name</th>
              <th>Surname</th>
              <th>Title</th>
            </tr>
          </thead>
        </table>
        {display}
      </div>
    );
  }
}

export default Dashboard;
